<?php
namespace App\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Cilex\Provider\Console\Command;

class MigrateCommand extends Command
{
    protected $entities = [
      'Entities\Admin',
      'Entities\Conference',
      'Entities\Mailman',
      'Entities\Profile',
      'Entities\Registration',
      'Entities\User'
    ]
    protected function configure()
    {
        $this
            ->setName('database:migrate')
            ->setDescription('Migrate Database Schema');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln("Migrating Database");

        $database = $this->getApplication()->getService('database');

        foreach($this->entities as $entity) {
          $output->writeln("Migrating $entity");
          $mapper = database->mapper($entity);
          $mapper->migrate();
        }

        $output->writeln("Migration Complete");
    }
}
